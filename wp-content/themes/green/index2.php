
<?php get_header();?>
<!--[if IE]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
<div class="container-fluid">
  <div class="row">
    <section class="intro-blog-list">
      <img class="intro-blog-list__bg-blog-list" src="<?php bloginfo('template_url'); ?>/images/bg-blog-list.png"> 
      <a href="index.html" class="pull-left header">
        <img class="header__logo"
     src="<?php bloginfo('template_url'); ?>/images/logofavicon.png" alt="greenhub">
      <h1 class="header__logo-text">greenhub</h1>
      </a>
      <div class="intro-blog-list__header-text">
        <h3 class="title">Top plants for bathrooms</h3>
        <p>Plant reminder and community for people who are obsessed with plants</p>
      </div>
    </section>
    <section class="read-about clearfix text-center">
      <h3 class="title text-center">Also read about</h3>
      <div class="read-about__cards text-center">
       
      <?php

          $args = array(
        'post_type'         => 'post',
        'posts_per_page'    => 10
    );
          $the_query = new WP_Query( $args );

       if ( $the_query->have_posts()) : while ( $the_query->have_posts()) : $the_query->the_post(); ?>

      	 <a href="<?php  echo the_permalink(); ?>" title="<?php echo the_title_attribute(); ?>" class="col-md-4 col-xs-12">
            <div class="read-about__cards__card">
             
              <?php the_post_thumbnail('post-thumbnail', ['class' => 'read-about__cards__card__list-img', 'title' => 'Feature image']);?>

              
              <span class="read-about__cards__card__categary"></span>
              <div class="read-about__cards__card__topic-header">
                  <?php   the_title("<h4>","</h4>"); ?>
                <p>
                  <?php  the_excerpt(); ?></p>
              
              </div>
            </div>
          </a>

      <?php endwhile; ?>

      <?php endif; ?>
      </div>
    </section>
    <section class="get-started text-center">
      <h3 class="title">Get started now !</h3><a href="#">
        <img class="get-started__download-btn" src="<?php bloginfo('template_url'); ?>/images/appstore.png"></a>
      </section>
<?php get_footer(); ?>
