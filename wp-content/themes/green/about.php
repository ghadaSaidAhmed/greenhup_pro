<?php /* Template Name: About us */ ?>




<?php get_header();?>

<div class="container-fluid">
  <div class="row">
    <section class="intro-blog-list">
      <img class="intro-blog-list__bg-blog-list" src="<?php bloginfo('template_url'); ?>/images/bg-blog-list.png"> 
      <a href="<?php echo site_url(); ?>" class="pull-left header">
        <img class="header__logo"
     src="<?php bloginfo('template_url'); ?>/images/logofavicon.png" alt="greenhub">
      <h1 class="header__logo-text">greenhub</h1>
      </a>
      <div class="intro-blog-list__header-text">
        <h3 class="title">Top plants for bathrooms</h3>
        <p>Plant reminder and community for people who are obsessed with plants</p>
      </div>
    </section>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
the_content();
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>

   




<?php get_footer();?>