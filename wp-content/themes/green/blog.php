<?php /* Template Name: Blog Page */ ?>

<?php get_header();?>
<!--[if IE]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
<h2> blog  khkjhkjj</h2>

    <div id="carousel-example-generic" class="carousel slide blog-list-carousel" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item ">
            <img src="<?php bloginfo('template_url'); ?>/images/img-list.png" alt="...">
            <div class="carousel-caption">
              <a href="#">THE BASICS OF PLANTING AND GROWING A VEGETABLE GARDEN</a>
            </div>
          </div>
          <div class="item">
            <img src="<?php bloginfo('template_url'); ?>/images/img-list.png" alt="...">
            <div class="carousel-caption">
              <a href="#">THE BASICS OF PLANTING AND GROWING A VEGETABLE GARDEN</a>
            </div>
          </div>
        </div>
      </div>



        <div class="container">
      <div class="row">
        <section class="read-about clearfix text-center">
          <h3 class="title text-center">Also read about</h3>


       <?php

          $args = array(
        'post_type'         => 'post',
        'posts_per_page'    => 10
    );
          $the_query = new WP_Query( $args );

       if ( $the_query->have_posts()) : while ( $the_query->have_posts()) : $the_query->the_post(); ?>


          <div class="col-xs-12 col-sm-6 col-md-4">
            <article class="post-single">
              <figure class="post-media">
                <a href="<?php  echo the_permalink(); ?>">
                  
                  <?php the_post_thumbnail('post-thumbnail', [ 'title' => 'Feature image']);?>
                </a>
              </figure>
              <div class="post-body">
                <div class="post-meta">
                    <div class="post-tags"><a href="#">
                      <?php
                           foreach((get_the_category()) as $category) 
                           {
                            echo  '<span>'.$category->cat_name . ' '.'</span>';
                        }
                        ?></a></div>
                    <div class="post-date"><?php   the_date(); ?></div>
                </div>
                <h4 class="dark-color text-left"><a href="<?php  echo the_permalink(); ?>"><?php   the_title(); ?></a></h4>
                <p class="dark-color text-left">
                  <?php  the_excerpt(); ?>
                </p>
              </div>
            </article>
          </div>

        <?php endwhile; ?>

      <?php endif; ?>

        </section>
      </div>
    </div>
 

  <?php get_footer(); ?>