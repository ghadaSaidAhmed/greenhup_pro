/*
Theme Name: Related Posts
Theme URI: https://www.cssigniter.com/ignite/programmatically-get-related-wordpress-posts-easily/
Description: Related Posts Tutorial (child theme of Twenty Fourteen)
Author: Anastis Sourgoutsidis
Author URI: https://www.cssigniter.com
Template: twentyfourteen
Version: 1.0.0
*/

@import url(../twentyfourteen/style.css);
