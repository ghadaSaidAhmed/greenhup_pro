

      <div class="subscribe-area section-padding">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
              <div class="subscribe-form text-center">
                <h3 class="green-color u-text-uppercase">Be the first one to use GreenHub</h3>
                <div class="space-20"></div>
                <form id="mc-form">
                  <input type="email" class="control" placeholder="Enter your email" required="required" id="mc-email">
                  <button class="bttn-white bttn-default active" type="submit"><span class="lnr lnr-location"></span> Subscribe</button>
                  <label class="mt10" for="mc-email"></label>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php  wp_footer();?>
  <footer class="footer-area" id="contact_page">
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-5">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <span>Copyright &copy;
              <script>document.write(new Date().getFullYear());</script> All rights reserved
              Greenhub</span>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <div class="space-30 hidden visible-xs"></div>
          </div>
          <div class="col-xs-12 col-md-7">
            <div class="footer-menu">
              <ul>
                <li><a href="#">Privacy</a></li>
                <li><a href="#">About</a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/blog">Blog</a></li>
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer-Bootom-End -->
  </footer>

  <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
  <script>
    (function (b, o, i, l, e, r) {
    b.GoogleAnalyticsObject = l; b[l] || (b[l] =
      function () { (b[l].q = b[l].q || []).push(arguments) }); b[l].l = +new Date;
      e = o.createElement(i); r = o.getElementsByTagName(i)[0];
      e.src = 'https://www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X'); ga('send', 'pageview');
  </script>
</body>

</html>
