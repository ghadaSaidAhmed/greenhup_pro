  <?php get_header();?>
<!--[if IE]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
 <nav class="mainmenu-area" data-spy="affix" data-offset-top="200">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary_menu">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img  src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Logo"></a>
      </div>
      <div class="collapse navbar-collapse" id="primary_menu">
        <ul class="nav navbar-nav mainmenu">
        </ul>
        <div class="right-button hidden-xs">
          <a href="#">Download the App</a>
        </div>
      </div>
    </div>
  </nav>

  <header class="home-area overlay" id="home_page">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 hidden-sm col-md-6">
          <figure class="mobile-image wow fadeInUp" data-wow-delay="0.2s">
            <img  src="<?php bloginfo('template_url'); ?>/images/greenhub-mockup.png" alt="">
          </figure>
        </div>
        <div class="col-xs-12 col-md-6">
          <h1 class="u-font-weight--bold">GreenHub app helps you taking care of your plants.</h1>
          <div class="space-20"></div>
          <div class="desc wow fadeInUp" data-wow-delay="0.6s">
            <p>we provides a watering reminder and a social network linking people who are interested
              in houseplants to share knowledge and help each other. </p>
          </div>
          <div class="space-20"></div>

          <a href="#" class="bttn-white wow fadeInUp" data-wow-delay="0.8s"><i class="lnr lnr-download"></i>Download
            App</a>
        </div>
      </div>
    </div>
  </header>

  <section class="progress-area" id="progress_page">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="page-title section-padding">
            <h5 class="title wow fadeInUp" data-wow-delay="0.2s">Our Goal</h5>
            <h3 class="dark-color wow fadeInUp" data-wow-delay="0.4s">Never forget to water your plants again!</h3>
            <div class="space-20"></div>
            <div class="desc wow fadeInUp" data-wow-delay="0.6s">
              <p>It’s easy to forget to water your plants, but it doesn’t have to be. Happy Plant reminds you to water plants through game-like notifications, plant-selfies, and time-lapse videos so you can watch your Happy Plants grow baby grow.</p>
            </div>
            <div class="space-50"></div>
            <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s">Learn More</a>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
          <figure class="mobile-image">
            <img  src="<?php bloginfo('template_url'); ?>/images/progress-phone.png" class="center-block" style="height: 700px;" alt="">
          </figure>
        </div>
      </div>
    </div>
  </section>

  <section class="feature-area section-padding-top" id="features_page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div class="page-title text-center">
              <h5 class="title">Features</h5>
              <div class="space-10"></div>
              <h3 class="u-text-uppercase">The best Features</h3>
              <div class="space-60"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="box-icon">
                <i class="lnr lnr-rocket"></i>
              </div>
              <h4>Watering reminder</h4>
              <p>Green hub helps you to take care of your plants and keep them healthy.</p>
            </div>
            <div class="space-60"></div>
            <div class="service-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="box-icon">
                <i class="lnr lnr-paperclip"></i>
              </div>
              <h4>Social community</h4>
              <p>Green hub helps you to take care of your plants and keep them healthy.</p>
            </div>
            <div class="space-60"></div>
            <div class="service-box wow fadeInUp" data-wow-delay="0.6s">
              <div class="box-icon">
                <i class="lnr lnr-cloud-download"></i>
              </div>
              <h4>Social community</h4>
              <p>Green hub helps you to take care of your plants and keep them healthy.</p>
            </div>
            <div class="space-60"></div>
          </div>
          <div class="hidden-xs hidden-sm col-md-4">
            <figure class="mobile-image">
              <img  src="<?php bloginfo('template_url'); ?>/images/feature-image.png" alt="Feature Photo">
            </figure>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="box-icon">
                <i class="lnr lnr-clock"></i>
              </div>
              <h4>Social community</h4>
              <p>Green hub helps you to take care of your plants and keep them healthy.</p>
            </div>
            <div class="space-60"></div>
            <div class="service-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="box-icon">
                <i class="lnr lnr-laptop-phone"></i>
              </div>
              <h4>Social community</h4>
              <p>Green hub helps you to take care of your plants and keep them healthy.</p>
            </div>
            <div class="space-60"></div>
            <div class="service-box wow fadeInUp" data-wow-delay="0.6s">
              <div class="box-icon">
                <i class="lnr lnr-cog"></i>
              </div>
              <h4>Social community</h4>
              <p>Green hub helps you to take care of your plants and keep them healthy.</p>
            </div>
            <div class="space-60"></div>
          </div>
        </div>
      </div>
    </section>

  <section class="video-area section-padding gray-bg">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="video-photo">
            <img  src="<?php bloginfo('template_url'); ?>/images/video-image.jpg" alt="">
            <a href="https://www.youtube.com/watch?v=rmJJgxXZB7I" class="popup video-button">
              <img  src="<?php bloginfo('template_url'); ?>/images/play-button.png" alt="">
            </a>
          </div>
        </div>
        <div class="col-xs-12 col-md-5 col-md-offset-1">
          <div class="space-60 hidden visible-xs"></div>
          <div class="page-title">
            <h5 class="title wow fadeInUp" data-wow-delay="0.2s">INTRO VIDEO</h5>
            <div class="space-10"></div>
            <h3 class="dark-color wow fadeInUp" data-wow-delay="0.4s">Forget to water your plants? We got your back.</h3>
            <div class="space-20"></div>
            <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                <p>We get it. We forget to water plants because it's boring. But our green babies need us and so we thought to change boring chores into fun tasks.</p>
            </div>
            <div class="space-50"></div>
            <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s">Learn More</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="row">
        <section class="read-about clearfix text-center">
          <h3 class="title text-center">Also read about</h3>


       <?php

          $args = array(
        'post_type'         => 'post',
        'posts_per_page'    => 10
    );
          $the_query = new WP_Query( $args );

       if ( $the_query->have_posts()) : while ( $the_query->have_posts()) : $the_query->the_post(); ?>


          <div class="col-xs-12 col-sm-6 col-md-4">
            <article class="post-single">
              <figure class="post-media">
                <a href="<?php  echo the_permalink(); ?>">
                  
                  <?php the_post_thumbnail('post-thumbnail', [ 'title' => 'Feature image']);?>
                </a>
              </figure>
              <div class="post-body">
                <div class="post-meta">
                    <div class="post-tags"><a href="#">
                    	<?php
                           foreach((get_the_category()) as $category) 
                           {
                            echo  '<span>'.$category->cat_name . ' '.'</span>';
                        }
                        ?></a></div>
                    <div class="post-date"><?php   the_date(); ?></div>
                </div>
                <h4 class="dark-color text-left"><a href="<?php  echo the_permalink(); ?>"><?php   the_title(); ?></a></h4>
                <p class="dark-color text-left">
                  <?php  the_excerpt(); ?>
                </p>
              </div>
            </article>
          </div>

        <?php endwhile; ?>

      <?php endif; ?>

        </section>
      </div>



    <?php get_footer(); ?>
