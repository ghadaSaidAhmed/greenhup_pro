

<?php get_header();?>
 <?php
    // Start the loop.
    while ( have_posts() ) : the_post(); ?>

  <div class="article-banner">
       
       <?php the_post_thumbnail('post-thumbnail', [ 'title' => 'Feature image']);?>

       <div class="article-banner__writer">
          <?php  the_title("<h2 class='article-banner__title'>","</h2>"); ?>
          <p>
            Our Vegetable Gardening Guide for beginners will help you to plan and grow your tastiest vegetables ever. Find out how much food you need to grow to feed a family, top 10 vegetables for a beginner, and more tips.
          </p>
          <?php echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size ); ?>
          <span><?php   the_author();  ?></span>
       </div>
    </div>


    <div class="container">
      <div class="row">
        <article>
            <div class="field-item even" property="schema:articleBody"><div id="first-para-wrapper"><p>Our <strong>Vegetable Gardening Guide for beginners&nbsp;</strong>will help you to plan and grow your tastiest vegetables ever. Find out how much food you need to grow to feed a family, top 10 vegetables for a beginner, and more&nbsp;tips.</p>
            </div><!-- Ad: Node Injected 336x280 Top -->
            <div id="dfp-ad-107802" class="sitewide-rectangle-ad">
            <script type="text/javascript">
            ybotq.push(function() {
              googletag.cmd.push(function() {
                googletag.display('dfp-ad-107802');
              });
            });
            </script>
            </div>
            
            </div>
        </article>
      </div>
    </div>

   <?php
          // End of the loop.
    endwhile;
    ?>

<?php get_footer(); ?>
