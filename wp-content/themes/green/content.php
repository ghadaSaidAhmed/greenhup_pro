<?php
/**
 * @package WordPress
 * @subpackage green hub
 */
?>
<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="col-md-4 col-xs-12">
	<div class="read-about__cards__card">

		<?php the_post_thumbnail('post-thumbnail', ['class' => 'read-about__cards__card__list-img', 'title' => 'Feature image']);?>
		<span class="read-about__cards__card__categary"><?php //the_category(); ?></span>
		<div class="read-about__cards__card__topic-header">
			<?php  the_title("<h4>","</h4>"); ?>
			
		</div>
	</div>
</a>
