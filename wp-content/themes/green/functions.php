<?php


/*----------- add style -----------*/
 function addStyle()
{
	wp_enqueue_style( 'my_vendor_css', get_template_directory_uri() .'/styles/vendor.css' );
	wp_enqueue_style( 'my_main_css', get_template_directory_uri() .'/styles/main.css' );

}
/*----------- add style -----------*/
 function addScript()
{
	wp_enqueue_script( 'my_modernizr', get_template_directory_uri() .'/scripts/vendor/modernizr.js' );

	/* footer sctipts */
	wp_enqueue_script( 'my_vendor_js', get_template_directory_uri() .'/scripts/vendor.js',array(),false,true );
	
	wp_enqueue_script( 'my_main_js', get_template_directory_uri() .'/scripts/main.js',array(),false,true );
	wp_enqueue_script( 'my_plugins_js', get_template_directory_uri() .'/scripts/plugins.js',array(),false,true );
	//wp_enqueue_script( 'my_modernizr', ,array(),false,true );
}

// slider

/*function bootstrapstarter_wp_setup() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'bootstrapstarter_wp_setup' );
*/
//end slider

/* hooks */
add_action( 'wp_enqueue_scripts', 'addStyle' );

add_action( 'wp_enqueue_scripts', 'addScript' );



/* feature  */

add_theme_support( 'post-thumbnails' );


/* css tricks */
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );


function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

/* content */
//add_filter("the_content", "plugin_myContentFilter");
 function plugin_myContentFilter($content)
 {
   /*if ( is_array( $pages ) ) {
		if ( $page > count( $pages ) ) // if the requested page doesn't exist
			$page = count( $pages ); // give them the highest numbered page that DOES exist
	} else {
		$page = 0;
	}
	*/
   // Take the existing content and return a subset of it
	if(!is_single())
	{
	return substr($content, 0, 50);
	
	}
	return $content;

   
 }
 
 //expert
 function custom_excerpt_length( $length ) {
        return 20;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


//load carsoul 

function load_scripts( $hook ) {

    global $post;

    if ( $hook == 'all-posts.php' ) {
           
            wp_enqueue_script(  'myscript', get_stylesheet_directory_uri().'/js/myscript.js' );
        
    }
}

add_action('wp_enqueue_scripts', 'load_scripts');
