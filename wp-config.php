<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '2f6c6a8239031cd0b22365e25614d23af2e511038fcf659b');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2;NGr(hpZyqF)Fbxo{J}R$/D#*tf$&md,LZ]xdF{z-x%U$B5Zzz<ix`MI(Wd<I}Y');
define('SECURE_AUTH_KEY',  'Yt0|&F<mK%@v j$(T-=>D,r2n#jjU/WW;&e4 n6nQxBS[)[2>^Af?U8`@r>8f}aT');
define('LOGGED_IN_KEY',    'rUx,Kk3RcRzqN=,/V~? JC&6:vTZCdb47=S4j4`p }-e>I(qSO|]>|0d[Jc%A`Dn');
define('NONCE_KEY',        '5eTHA,KR$1 lCeT?!ELnrF+5Q=gn^oPL7a7]j %*_BTNdlStfQLp)-?qpIDH;M0#');
define('AUTH_SALT',        'D?I#z#T.v`wehS@x/Bcqj&CWWKqH,mTfoi[7V6oA~v7U9^(0KI3{mKkOI]^x0MF.');
define('SECURE_AUTH_SALT', 'ES>3Fq`p!m)w2<E#Y?.N%y`aJBpKaCA-v>]_ 6hi1GL=0Zr1Op(A(EL3AMK!W^e7');
define('LOGGED_IN_SALT',   '!,{{_J2S(Nsv5c50eB4J$Cuc@}H:fnXOG?G#z{sT#9SF]-oSy/Z.bdykXD_nE5XC');
define('NONCE_SALT',       'w(JLO]%.JLlA(S#0Ge760+9D4DWDS2kiwOU&iWJ_0H@`NA?}o|hKPZp;{mY=JeXT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
